# first-php

## Exercices

### Exercice POO Person ([class](src/Person.php)/[entry point](public/exo-person.php))
1. Créer dans le dossier src un fichier Person.php et dedans faire une classe Person qui aura comme propriété un $firstName en string et un $name en string et un namespace App
2. Générer un constructeur pour cette classe (et faire que le constructeur attende le nom et le prénom donc)
3. Créer un nouveau point d'entrée dans public, qu'on va appeler exo-person.php et dedans faire un ptit require du vendor/autoload comme on a fait dans l'exemple
4. Dans ce point d'entrée faire 2-3 instances de la classe Person avec des noms et prénom différents
5. Dans la classe Person, rajouter une méthode introduction() qui va faire un echo de Hello, my name is suivi du nom et du prénom. Appeler cette méthode sur les différentes instances
6. Rajouter une autre méthode greets qui va attendre une Person en argument et qui va faire un echo de Hello {le nom de la person en argument}, how are you ? 

### TodoList OOP ([classes](src/Todo/)/[entry point](public/exo-todo.php))
1. Créer un dossier Todo  dans src et dedans créer une classe Task qui aura comme propriété (privées) un label en string et un done en boolean
2. Faire un constructeur pour cette classe qui va attendre juste un label et faire que le done soit initialisé à false
3. Créer une méthode toggle() qui va passer la valeur du done à l'inverse de sa valeur actuelle (donc de true à false ou de false à true)
4. Faire un ptit point d'entrée dans public, genre exo-todo.php, et y mettre le require et créer une instance de Task voir si tout ce qu'on a fait fonctionne ou pas (pouvez le faire avec le debugger)
5. Créer une classe TodoList qui aura comme propriété un array initialisé vide (ça sera un Task[] techniquement, vous pouvez le mettre en documentation) , et pas de constructeur
6. Rajouter une méthode addTask qui attendra un label en argument et fera une nouvelle instance de Task et la mettra dans le tableau
7. Créer une méthode getTask qui attendra un index et qui renverra la task associé dans le tableau, avec une vérification qu'il y a bien quelque chose à l'index donné, sinon renvoyer null
8. Faire une instance de TodoList dans le point d'entrée, et s'amuser autant que possible avec
9. Dans la classe Task rajouter une méthode toHTML qui va soit renvoyer soit afficher une chaîne de caractère représentant la task (donc avec son label et pis un input coché ou non)
10. Dans la classe TodoList, rajouter aussi un toHTML qui va boucler sur toutes les Task avec un ptit foreach, et pour chaque task, déclencher sa méthode toHTML pour faire en sorte que ça affiche toutes les la TodoList 
