<?php
//Le use est l'équivalent du import {} from '...' en JS/TS
use App\First;
//Le require vendor/autoload est nécessaire pour que PHP charge automatiquement les 
//fichiers nécessaire en se basant sur les use et les namespace. On a besoin de le mettre
//pour chaque point d'entrée (les fichiers php à l'intérieur du dossier public, ceux qui
//lancent le code)
require '../vendor/autoload.php';

//Ici on fait une instance de la classe First et via son constructeur on initialise count à 0
$instance = new First(0);
//On lance la méthode increment, ce qui va la déclencher spécifiquement pour la première instance dont le count va passer à 1
$instance->increment();

//On fait une deuxième instance qui aura son propre count initialisé à 5 (différent du count de la première instance qui lui vaut toujours 1)
$instance2 = new First(5);
//On increment le count de la deuxième instance qui passe à 6
$instance2->increment();
//Ici, ça affichera le count de la première instance, donc 1
echo $instance->count;

//version js 
/*
let instance = {count:0};
instance.count = 10;
console.log(instance.count);
*/