<?php
use App\Todo\TodoList;

require '../vendor/autoload.php';


$list = new TodoList();

$list->addTask("Faire un truc");
$list->addTask("Faire un autre truc");
$list->addTask("Faire un dernier ptit truc");

$list->getTask(1)?->toggle();

echo $list->toHTML();