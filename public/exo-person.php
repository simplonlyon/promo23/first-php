<?php
use App\Person;

require '../vendor/autoload.php';

$person1 = new Person("Demel", "Jean");
$person2 = new Person("Soumare", "Rama");
$person3 = new Person("Garcia", "Annaëlle");


$person2->introduction();

$person1->greets($person3);