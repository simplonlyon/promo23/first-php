<?php
//toutes les variables en php doivent commencer par un $
//Au niveau des conventions de nommages sinon c'est comme en JS, camelCase pour les variable et function
//Techniquement il n'y a pas de mot clef pour déclarer une variable, la variable commence à exister quand on lui assigne une valeur une première fois
$nomVariable = 'initial';
//Les tableaux marchent de la même manière en PHP, sauf que ce n'est pas des objets, il faudra donc appeler des fonctions comme en dessous pour certaines opérations
$tab = [1,2,3,4];

$tab[] = 5; //équivalent de tab.push(5) en js
array_push($tab, 6); //équivalent de la ligne du dessus

echo $tab[0]; //pour accéder ou modifier la valeur c'est comme en js

//Pas de différence avec les if. Le isset est une fonction qui vérifie si une variable existe
if (isset($nomVariable)) {
    echo $nomVariable;
} else {
    echo 'pas de variable';
}

echo add(5.7, 10);
//for i classique, pas de différence
for ($i=0; $i < 10; $i++) {
    echo $i;
}

//équivalent du for..of qui attend en premier argument le tableau et le nom de l'itérateur
//en deuxième, on peut également récupérer l'index en écrivant `as $index => $item`
foreach ($tab as $item) {
    echo $item;
}
//En PHP les objets inline n'existent pas comme en javascript, on peut avoir un résultat
//à peu près similaire en utilisant des tableaux dit "associatifs", où on indique une
//chaîne de caractère comme index et la valeur à assigner à l'index en question
$dog = [
    'name' =>  'fido',
    'breed' => 'corgi',
    'age' => 10
];
//Ils s'utilisent comme des tableaux (et on peut faire des foreach dessus)
echo $dog['name'];

/**
 * Les fonctions marchent de la même manière qu'en TS, on peut
 * typer les paramètres ainsi que le type de retour et faire de la PHP doc
 * (petite différence au passage ne PHP on a pas de number, on a int pour les entier et float pour les nombres à virgules)
 * @param float $a premier nombre
 * @param float $b deuxième nombre
 * @return float résultat de l'addition
 */
function add(float $a, float $b): float {
    return $a + $b;
}



/*
//exemple de ternaire
$maVariable = isset($nomVariable) ? 'bloup' : 'blip';
//ça fait pareil que ça
$maVariable;
if(isset($nomVariable)) {
    $maVariable = 'bloup';
} else {
    $maVariable = 'blip';
}
*/