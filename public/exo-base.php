<?php

$tabNames = ['Jean', 'Rama', 'Annaëlle'];
displayNames($tabNames);


function nameToPara(string $name):string {
    $charCount = strlen($name);
    return "<p>$name : $charCount</p>";
}


function displayNames(array $names) {
    foreach($names as $item) {
        echo nameToPara($item);
        
    }
}