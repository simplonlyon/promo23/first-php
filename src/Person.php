<?php

namespace App;

class Person {
    private string  $name;
    private string $firstName;

    /**
     * @param string $name
     * @param string $firstName
     */
    public function __construct(string $name, string $firstName) {
    	$this->name = $name;
    	$this->firstName = $firstName;
    }

    public function introduction():void {

        echo "Hello my name is {$this->firstName} {$this->name}";
        $this->name = 'bloup';
    }

    public function greets(Person $target): void {
        echo "Hello {$target->firstName} how are you ?";
        
    } 
}