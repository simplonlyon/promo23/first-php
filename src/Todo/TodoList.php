<?php

namespace App\Todo;

class TodoList {
    /**
     * @var Task[] Les Tasks contenues dans la TodoList
     */
    private array $tasks = [];

    /**
     * Méthode permettant d'ajouter une nouvelle Task dans la liste
     * @param string $label Le label de la Task à ajouter
     */
    public function addTask(string $label): void {
        $task = new Task($label);
        $this->tasks[] = $task;
    }

    /**
     * Méthode permettant de récupérer une Task de la liste en se basant sur son index
     * avec vérification de si la task existe ou pas
     * @param int $index l'index de la Task voulue
     * @return Task|null On renvoie l'instance de Task correspondante ou null si pas de Task trouvée à l'index indiqué
     */
    public function getTask(int $index):?Task {
        if(isset($this->tasks[$index])) {
            return $this->tasks[$index];

        }
        return null;
    }

    /**
     * Méthode qui génère le HTML de la TodoList en se basant sur les tasks qu'elle a 
     * actuellement
     * @return string Les balises HTML générées
     */
    public function toHTML():string {
        //On crée une string qui contiendra tout le html généré
        $html = '<ul>';
        //Pour chaque Task contenue dans la liste, on concatène son html (avec la méthode de la task) dans la variable
        foreach ($this->tasks as $item) {
            $html .= $item->toHTML();
        }
        $html .= '</ul>';
        return $html;
    }
}