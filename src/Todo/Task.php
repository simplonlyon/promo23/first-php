<?php

namespace App\Todo;

class Task {
    private string $label;
    private bool $done;

    /**
     * @param string $label Le label de la tâche à accomplir
     */
    public function __construct(string $label) {
    	$this->label = $label;
    	$this->done = false;
    }

    /**
     * Méthode permettant de passer le done de true à false ou de false à true
     * @return void
     */
    public function toggle():void {
        $this->done = !$this->done;
        // if($this->done) {
        //     $this->done = false;
        // } else {
        //     $this->done = true;
        // }
    }

    /**
     * Méthode qui génère le HTML pour la Task qui l'exécute avec une checkbox cochée ou pas
     * @return string Le HTML généré, ici un li
     */
    public function toHTML():string {
        //On utilise un ptit ternaire pour mettre soit "checked" soit rien dans une chaîne de caractères
        $checked = $this->done?'checked':'';
        //ce qui nous permet de faire que l'input généré soit coché ou non
        return "<li>{$this->label} <input type=\"checkbox\" {$checked}></li>";
    }
}