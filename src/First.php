<?php

/**
 * Le namespace est une représentation du chemin où se trouve la classe déclaré.
 * Tout ce qui se trouve dans src aura un namespace, et le namespace aura comme nom
 * initial ce qu'on a déclaré dans le composer.json (ici on a dit que le dossier src/ aura
 * comme namespace App)
 */
namespace App;
/**
 * Une classe représente un modèle pour créer des objets, ça se rapproche vaguement
 * des interfaces avec comme principale différence qu'on peut déclarer et créer des
 * méthodes dans les classes (et initialiser des valeurs aux propriétés, alors que dans
 * les interfaces on ne peut que les typer)
 * On utilisera pas la classe directement pour appeler ses méthodes, à la place on 
 * passera par les instances de celle ci, c'est les instances qui appeleront les méthodes (voir public/example-oop.php)
 */
class First {
    /**
     * Ici on délcare une propriété de la classe First, cette propriété pourra être
     * accéder à l'intérieur de la classe grâce au mot clef $this et comme elle est publique
     * elle pourra également être accédée depuis l'instance
     */
    public int $count;

    /**
     * Le constructeur est la méthode qui est lancée au moment où on fait une 
     * nouvelle instance de la classe en question (ici, quand on fera un new First())
     * C'est une méthode spéciale qui sert à initialiser les propriétés des instances
     * de la classe
     */
    public function __construct(int $count) {
    	$this->count = $count;
    }
    /**
     * Une méthode est une fonction à l'intérieur d'une classe. Cette fonction sera
     * appelée par une instance et ce qui se trouve à l'intérieur ne sera donc appliquée
     * que pour l'instance qui a appelé la méthode en question. C'est à dire que si
     * je fais un increment() sur une première instance, ça ne changera la valeur de la
     * propriété count que pour la première instance et pas pour les autres
     */
    public function increment() {
        $this->count++;
    }
}
